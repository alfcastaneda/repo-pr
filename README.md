# PRs

This is a simple app in angular to show the pull requests in a repo.
At the moment this have a hardcoded value to search for angular repo PR's.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.


## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.



## Further Contact

Reach me by mail alfcastaneda@gmail.com.
