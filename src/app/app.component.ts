import { Component, OnInit } from '@angular/core';
import { PullRequestsService } from './pull-requests.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
@Component({
  selector: 'app-root',
  template: `
    <header>
      <mat-toolbar>
        <h2>{{title}} on {{repoName}} repo of {{userName}}</h2>
      </mat-toolbar>
    </header>
    <nav class="nav">
      <mat-form-field>
        <input matInput placeholder="Search PR" #name (keyup)="searchPR(name.value)">
      </mat-form-field>
      <mat-form-field>
        <mat-select #state (change)="changeState(state.value)" [(value)]="statePR">
          <mat-option value="">All</mat-option>
          <mat-option value="open">Open</mat-option>
          <mat-option value="closed">Closed</mat-option>
        </mat-select>
      </mat-form-field>
    </nav>
    <div class="container">
     <app-pull-request *ngFor="let pr of pullRequests$ | async" [pr]="pr"></app-pull-request>
    </div>
    <div class="nav-btns">
      <button  *ngIf="pulls.links?.prev" (click)="prev()" mat-raised-button>PREVIOUS PAGE</button>
      <button  *ngIf="pulls.links?.next" (click)="next()" mat-raised-button>NEXT PAGE</button>
    </div>
  `,
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'Pull Requests';
  userName = 'angular';
  repoName = 'angular';
  statePR = '';
  pullRequests$: Observable<Object[]>;
  constructor(private pulls: PullRequestsService) {}

  ngOnInit() {
    this.pullRequests$ = this.pulls.getPullRequests$();
    this.start(this.userName, this.repoName);
  }

  start(userName: string, repoName: string) {
    this.pulls.setUserAndRepo(userName, repoName);
  }

  next() {
    this.pulls.nextPage();
  }

  searchPR(value: string) {
    this.pulls.filterByName(value);
  }

  changeState(value: string) {
    this.pulls.changeState(value);
  }


}
