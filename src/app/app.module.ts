import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from './material.module';

import { AppComponent } from './app.component';
import { PullRequestComponent } from './pull-request.component';
import { PullRequestsService } from './pull-requests.service';


@NgModule({
  declarations: [
    AppComponent,
    PullRequestComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule
  ],
  providers: [PullRequestsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
