import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-pull-request',
  template: `
    <mat-card class="pr" *ngIf="pr">
      <mat-card-header>
        <img mat-card-avatar class="example-header-image" src="{{pr.user.avatar_url}}"/>
        <mat-card-title>{{pr.title}}</mat-card-title>
        <mat-card-subtitle>{{pr.user.login}} <span [ngClass]="[pr.state, 'state']" class="state">{{pr.state}}</span>
        </mat-card-subtitle>
      </mat-card-header>
      <mat-card-content>
        Created: {{pr.created_at | date: 'mediumDate' }}
      </mat-card-content>
      <mat-card-actions>
        <mat-chip-list>
          <mat-chip *ngFor="let topic of pr.topics ">{{topic}}</mat-chip>
        </mat-chip-list>
        <a mat-raised-button href="{{pr.html_url}}" color="primary" target="_blank">Go to PR</a>
      </mat-card-actions>
    </mat-card>
  `,
  styleUrls: ['./pull-request.scss']
})

export class PullRequestComponent implements OnInit {
  @Input() pr: Object;
  constructor() { }

  ngOnInit() { }
}
