import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/observable/combineLatest';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/debounceTime';

interface Links {
  next?: string;
  last?: string;
  prev?: string;
  first?: string;
}

@Injectable()
export class PullRequestsService {
  links: Links;
  baseUrl = 'https://api.github.com/search/issues?type=pr';
  titleFilterSub = new BehaviorSubject('');
  statusFilterSub = new BehaviorSubject('');
  usrRepoSubject = new BehaviorSubject('');
  urlSubject = new BehaviorSubject('');
  response$ = Observable.combineLatest(
    this.usrRepoSubject,
    this.statusFilterSub,
    this.titleFilterSub.debounceTime(400),
    this.urlSubject
  );

  constructor(private httpClient: HttpClient) { }

  getPullRequests$(): Observable<any> {
    return this.response$
      .switchMap(([usrRepo, state, title, url]) => {
        let params, reqUrl;
        if (url && (!state || !title)) {
          params = {};
          reqUrl = url;
        } else {
          reqUrl = this.baseUrl;
          params = { q: `repo:${usrRepo}` }
          if (state) {
            params.q = params.q.concat(` state:${state}`);
          }
          if (title) {
            params.q = params.q.concat(` ${title} in:tilte`);
          }
        }
        return this.httpClient.get<any>(reqUrl, {observe: 'response', params});
      })
      .do(response => this.setLinks(response.headers.get('Link')))
      .map(response => response.body.items);
  }

  setLinks(links) {
    const rel = /[\\"]|(rel=)/g;
    this.links = links
      .replace(/[<> ]/g, '')
      .split(',')
      .reduce((acc, value) => {
        const arrayValue = value.split(';');
        const linkRef = arrayValue.filter(v => v.search(rel) >= 0)
          .map(v => v.replace(rel, '')).join('');
        const link = arrayValue.filter(v => v.search(rel) < 0)
          .join('');
        Object.assign(acc, {[linkRef]: link});
        return acc;
      }, {});
  }

  setUserAndRepo(userName: string, repoName: string) {
    const repo = `${userName}/${repoName}`;
    this.usrRepoSubject.next(repo);
  }

  filterByName(name: string) {
    this.titleFilterSub.next(name);
  }

  changeState(state: string) {
    this.statusFilterSub.next(state);
  }

  nextPage() {
    console.log(this.links);
    if (this.links.next) {
      this.urlSubject.next(this.links.next);
    }
  }
}

